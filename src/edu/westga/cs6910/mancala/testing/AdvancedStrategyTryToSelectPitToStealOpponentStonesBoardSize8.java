package edu.westga.cs6910.mancala.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6910.mancala.model.strategies.AdvancedStrategy;

/**
 * Tests the AdvancedStrategy class' second priority when selected a pit, which should
 * 	select the pit closest to the store that steals an opponents' stones when the board size is 8
 * 
 * @author Brandon Lee Upshaw - CS6910
 * @version July 20th, 2016
 *
 */
public class AdvancedStrategyTryToSelectPitToStealOpponentStonesBoardSize8 {
	private AdvancedStrategy theStrategy;
	
	/**
	 * Instantiates the instance variable for all tests in this class
	 */
	@Before
	public void setUp() {
		this.theStrategy = new AdvancedStrategy();
	}

	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberFiveToStealOpponentStones() {
		int[] theBoard = {2, 0, 0, 0, 0, 1, 0, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberFourToStealOpponentStones() {
		int[] theBoard = {0, 3, 0, 0, 1, 0, 0, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones - before it defaults to selecting the pit closest
	 * 	to the store which has no advantage of play
	 */
	@Test
	public void testShouldReturnPitNumberFourAndNotDefaultToNumberSix() {
		int[] theBoard = {0, 2, 0, 0, 1, 0, 4, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}	
	/**
	 * Test situation where stealing an opponents stones are not an option
	 * 	and strategy should default to selecting a stone closest to the store
	 */
	@Test
	public void testShouldReturnPitNumberFiveAfterGoingThroughPreviousStrategyOptions() {
		int[] theBoard = {1, 2, 3, 0, 4, 3, 0, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 * 	and ending in the starting pit
	 */
	@Test
	public void testShouldReturnPitFourToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 0, 7, 1, 3, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 * 	and ending in the starting pit
	 */
	@Test
	public void testShouldReturnPitFiveToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 0, 0, 7, 2, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 * 	and ending in the starting pit
	 */
	@Test
	public void testShouldReturnPitSixToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 0, 0, 1, 7, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will steal an opponent's stones
	 * 	after wrapping around the board.
	 */
	@Test
	public void testShouldReturnPitSixAfterWrappingToStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 0, 2, 0, 6, 3};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will land in a pit to steal
	 * 	an opponent's stones - when that pit was empty at the start of the turn
	 * 	and only has a stone because the ComputerPlayer wrapped the board and placed
	 * 	one there
	 */
	@Test
	public void testShouldReturnPitFiveAfterWrappingWhenAdjacentPitWasEmptyAtStartOfTurn() {
		int[] theBoard = {1, 3, 0, 0, 0, 6, 2, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where pit four steals opponents stones, but there 
	 * 	are pits closer to the store that contain stones
	 */
	@Test
	public void testShouldReturnPitFourToStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 0, 2, 3, 0, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	// Large Board Size Tests
	
	/**
	 * Test situation for extreme board size (30 pits) to verify that
	 * 	pit that lands in store after looping is selected
	 */
	@Test
	public void testShouldReturnPitFifteenBecauseItLoopsAndLandsInStore() {
		int[] theBoard = {1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 41, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 0};
		assertEquals(15, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation for extreme board size (30 pits) to verify that
	 * 	pit that lands in store after looping is selected
	 */
	@Test
	public void testShouldReturnPitSixteenBecauseItLoopsAndLandsInStore() {
		int[] theBoard = {1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 0, 27, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 0};
		assertEquals(16, this.theStrategy.selectPit(theBoard));
	}
}
