package edu.westga.cs6910.mancala.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6910.mancala.model.strategies.AdvancedStrategy;

/**
 * Tests methods in the AdvancedStrategy class for the Mancala Application
 * 
 * @author Brandon Lee Upshaw - CS6910
 * @version July 20th, 2016
 *
 */
public class AdvancedStrategyTryToSelectPitToEndInStore {
	private AdvancedStrategy theStrategy;

	/**
	 * Initializes JUnit instance variable to simplify testing
	 */
	@Before
	public void setUp() {
		this.theStrategy = new AdvancedStrategy();
	}

	/**
	 * Test for the situation where only the pit closest
	 * 	to the store has a stone
	 */
	@Test
	public void testShouldReturnClosestPit() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 1, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the closest pit has
	 * 	only 1 stone, and the second closest pit has 2
	 * 	stones
	 */
	@Test
	public void testShouldReturnPitWithSingleStone() {
		int[] theBoard = {0, 0, 0, 0, 0, 2, 1, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the second closest
	 * 	pit has 2 stones, and should be played
	 */
	@Test
	public void testShouldReturnSecondClosestPitWithTwoStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 2, 0, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the third closest pit has 
	 * 	3 stones, and should be played
	 */
	@Test
	public void testShouldReturnThirdClosestPitWithThreeStones() {
		int[] theBoard = {0, 0, 0, 0, 3, 0, 0, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where each pit contains number of 
	 * 	stones that would end turn in store on first move
	 */
	@Test
	public void testShouldReturnClosestPitFirst() {
		int[] theBoard = {0, 0, 0, 0, 3, 2, 1, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where closest pit is empty, but second
	 * 	and third pit contain stones that would end turn in store
	 */
	@Test
	public void testShouldReturnSecondClosestPitFirst() {
		int[] theBoard = {0, 0, 0, 0, 3, 2, 0, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Tests for situation where no playable pits would end
	 * 	the turn in the store, and closest pit with stones is played
	 */
	@Test
	public void testShouldReturnClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 2, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where no playable pits would end
	 * 	the turn in the store, and the second closest pit is played
	 */
	@Test
	public void testShouldReturnSecondClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 3, 0, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where no playable pits would end 
	 * 	the turn in the store, and the third closest pit is played
	 */
	@Test
	public void testShouldReturnThirdClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 4, 0, 0, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the forth pit would end in the 
	 * 	ComputerPlayer's store after looping the entire board, which
	 * 	would still award a free turn.
	 */
	@Test
	public void testShouldReturnPitNumberFourToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {0, 0, 0, 0, 10, 3, 2, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the fifth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberFiveToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {1, 1, 1, 0, 1, 9, 2, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the sixth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberSixToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {1, 1, 1, 0, 5, 6, 8, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the forth pit would end in the 
	 * 	ComputerPlayer's store after looping the entire board twice, which
	 * 	would still award a free turn.
	 */
	@Test
	public void testShouldReturnPitNumberFourToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {0, 0, 0, 0, 17, 3, 2, 0};
		assertEquals(4, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the fifth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberFiveToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {1, 1, 1, 0, 1, 16, 2, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the sixth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberSixToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {1, 1, 1, 0, 5, 6, 15, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
}
