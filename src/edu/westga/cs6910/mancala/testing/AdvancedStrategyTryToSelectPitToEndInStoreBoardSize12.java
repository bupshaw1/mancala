package edu.westga.cs6910.mancala.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6910.mancala.model.strategies.AdvancedStrategy;

/**
 * Tests the AdvancedStratgy class to verify that the strategy plays correctly
 * 	with a board size of 12
 * @author Brandon Lee Upshaw - CS6910
 * @version July 20th, 2016
 *
 */
public class AdvancedStrategyTryToSelectPitToEndInStoreBoardSize12 {
	private AdvancedStrategy theStrategy;

	/**
	 * Initializes JUnit instance variable to simplify testing
	 */
	@Before
	public void setUp() {
		this.theStrategy = new AdvancedStrategy();
	}

	/**
	 * Test for the situation where only the pit closest
	 * 	to the store has a stone
	 */
	@Test
	public void testShouldReturnClosestPit() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0};
		assertEquals(10, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the closest pit has
	 * 	only 1 stone, and the second closest pit has 2
	 * 	stones
	 */
	@Test
	public void testShouldReturnPitWithSingleStone() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 0};
		assertEquals(10, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the second closest
	 * 	pit has 2 stones, and should be played
	 */
	@Test
	public void testShouldReturnSecondClosestPitWithTwoStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the third closest pit has 
	 * 	3 stones, and should be played
	 */
	@Test
	public void testShouldReturnThirdClosestPitWithThreeStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the fourth closest pit has
	 * 	4 stones, and should be played
	 */
	@Test
	public void testShouldReturnForthClosestPitWithFourStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the fifth closest pit has 
	 * 	5 stones and should be played
	 */
	@Test
	public void testShouldReturnFifthClosestPitWithFiveStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where each pit contains number of 
	 * 	stones that would end turn in store on first move
	 */
	@Test
	public void testShouldReturnClosestPitFirst() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 5, 4, 3, 2, 1, 0};
		assertEquals(10, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where closest pit is empty, but all remaining
	 * 	pits contain stones that would end turn in store
	 */
	@Test
	public void testShouldReturnSecondClosestPitFirst() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 5, 4, 3, 2, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where closest two pits are empty, but 
	 * 	all remaining pits contain stones that would end turn in store
	 */
	@Test
	public void testShouldReturnThirdClosesPitFirst() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 5, 4, 3, 0, 0, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where closest three pits are empty, but
	 * 	all remaining pits contain stones that would end turn in store
	 */
	@Test
	public void testShouldReturnForthClosestPitFirst() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 5, 4, 0, 0, 0, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Tests for situation where no playable pits would end
	 * 	the turn in the store, and closest pit with stones is played
	 */
	@Test
	public void testShouldReturnClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0};
		assertEquals(10, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where no playable pits would end
	 * 	the turn in the store, and the second closest pit is played
	 */
	@Test
	public void testShouldReturnSecondClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where no playable pits would end 
	 * 	the turn in the store, and the third closest pit is played
	 */
	@Test
	public void testShouldReturnThirdClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where no playable pits would end their
	 * 	turn in the store, and the fourth closest pit is played
	 */
	@Test
	public void testShouldReturnForthClosestPitBecauseItHasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where no playable pits would end
	 * 	the turn in the store, and the fifth closest pit is played
	 */
	@Test
	public void testShouldReturnFifthClosestPitBecauseIthasStones() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the sixth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberSixToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {4, 0, 1, 4, 2, 8, 16, 0, 12, 9, 2, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the seventh pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberSevenToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {10, 2, 4, 9, 1, 1, 1, 15, 4, 11, 8, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the eighth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberEightToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {11, 4, 6, 9, 1, 3, 1, 0, 14, 1, 10, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the ninth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberNineToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {11, 4, 8, 9, 1, 1, 11, 0, 2, 13, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the tenth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberTenToGetFreeTurnAfterLoopingTheBoard() {
		int[] theBoard = {11, 4, 24, 9, 1, 1, 9, 0, 2, 1, 12, 0};
		assertEquals(10, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the sixth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberSixToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {4, 0, 1, 4, 2, 8, 27, 0, 12, 9, 2, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the seventh pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberSevenToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {10, 2, 4, 9, 1, 1, 1, 26, 4, 11, 8, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the eighth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberEightToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {11, 4, 6, 9, 1, 3, 1, 0, 25, 1, 10, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the ninth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberNineToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {11, 4, 8, 9, 1, 1, 11, 0, 2, 24, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for situation where the tenth pit would end in the ComputerPlayer's
	 * 	store after looping the entire board twice, which would still award a free turn
	 */
	@Test
	public void testShouldReturnPitNumberTenToGetFreeTurnAfterLoopingTheBoardTwice() {
		int[] theBoard = {11, 4, 24, 9, 1, 1, 9, 0, 2, 1, 23, 0};
		assertEquals(10, this.theStrategy.selectPit(theBoard));
	}
}	
	