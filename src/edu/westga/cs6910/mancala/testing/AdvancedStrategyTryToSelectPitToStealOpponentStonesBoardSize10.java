package edu.westga.cs6910.mancala.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6910.mancala.model.strategies.AdvancedStrategy;

/**
 * Tests the AdvancedStrategy class' second priority when selected a pit, which should
 * 	select the pit closest to the store that steals an opponents' stones when the board size
 * 	is 10
 * 
 * @author Brandon Lee Upshaw - CS6910
 * @version July 20th, 2016
 *
 */
public class AdvancedStrategyTryToSelectPitToStealOpponentStonesBoardSize10 {
	private AdvancedStrategy theStrategy;
	
	/**
	 * Instantiates the instance variable for all tests in this class
	 */
	@Before
	public void setUp() {
		this.theStrategy = new AdvancedStrategy();
	}

	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberSevenToStealOpponentStones() {
		int[] theBoard = {2, 0, 0, 0, 0, 0, 0, 1, 0, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberSixToStealOpponentStones() {
		int[] theBoard = {0, 3, 0, 0, 0, 0, 1, 0, 0, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberFiveToStealOpponentStones() {
		int[] theBoard = {0, 0, 3, 0, 0, 1, 0, 0, 0, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones - before it defaults to selecting the pit closest
	 * 	to the store which has no advantage of play
	 */
	@Test
	public void testShouldReturnPitNumberFiveAndNotDefaultToNumberEight() {
		int[] theBoard = {0, 0, 3, 0, 0, 1, 0, 0, 4, 0};
		assertEquals(5, this.theStrategy.selectPit(theBoard));
	}	
	/**
	 * Test situation where stealing an opponents stones are not an option
	 * 	and strategy should default to selecting a stone closest to the store
	 */
	@Test
	public void testShouldReturnPitNumberSevenAfterGoingThroughPreviousStrategyOptions() {
		int[] theBoard = {0, 0, 1, 2, 3, 5, 4, 3, 0, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 */
	@Test
	public void testShouldReturnPitSixToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 1, 1, 0, 9, 4, 2, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 */
	@Test
	public void testShouldReturnPitSevenToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 1, 1, 0, 0, 9, 2, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 */
	@Test
	public void testShouldReturnPitEightToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 1, 1, 7, 5, 3, 9, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will land in a pit to steal
	 * 	an opponent's stones - when that pit was empty at the start of the turn
	 * 	and only has a stone because the ComputerPlayer wrapped the board and placed
	 * 	one there
	 */
	@Test
	public void testShouldReturnPitSixAfterWrappingWhenAdjacentPitWasEmptyAtStartOfTurn() {
		int[] theBoard = {0, 0, 0, 0, 0, 0, 8, 3, 2, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will land in a pit to steal
	 * 	an opponent's stones - when that pit was empty at the start of the turn
	 * 	and only has a stone because the ComputerPlayer wrapped the board and placed
	 * 	one there
	 */
	@Test
	public void testShouldReturnPitSevenAfterWrappingWhenAdjacentPitWasEmptyAtStartOfTurn() {
		int[] theBoard = {0, 0, 0, 0, 0, 1, 0, 8, 2, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}

}
