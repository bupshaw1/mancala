package edu.westga.cs6910.mancala.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6910.mancala.model.strategies.AdvancedStrategy;

/**
 * Tests the AdvancedStrategy class' second priority when selected a pit, which should
 * 	select the pit closest to the store that steals an opponents' stones when the board size
 * 	is 12
 * 
 * @author Brandon Lee Upshaw - CS6910
 * @version July 20th, 2016
 *
 */
public class AdvancedStrategyTryToSelectPitToStealOpponentStonesBoardSize12 {
	private AdvancedStrategy theStrategy;
	
	/**
	 * Instantiates the instance variable for all tests in this class
	 */
	@Before
	public void setUp() {
		this.theStrategy = new AdvancedStrategy();
	}

	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberNineToStealOpponentStones() {
		int[] theBoard = {2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberEightToStealOpponentStones() {
		int[] theBoard = {0, 3, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberSevenToStealOpponentStones() {
		int[] theBoard = {0, 0, 3, 0, 0, 0, 0, 1, 0, 0, 0, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones
	 */
	@Test
	public void testShouldReturnPitNumberSixToStealOpponentStones() {
		int[] theBoard = {0, 0, 0, 3, 0, 0, 1, 0, 0, 0, 0, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the ComputerPlayer should make the selection
	 * 	that ends the turn in an empty pit adjacent to an opponents pit
	 * 	containing stones - before it defaults to selecting the pit closest
	 * 	to the store which has no advantage of play
	 */
	@Test
	public void testShouldReturnPitNumberSixAndNotDefaultToNumberEight() {
		int[] theBoard = {0, 0, 0, 3, 0, 0, 1, 0, 0, 0, 4, 0};
		assertEquals(6, this.theStrategy.selectPit(theBoard));
	}	
	/**
	 * Test situation where stealing an opponents stones are not an option
	 * 	and strategy should default to selecting a stone closest to the store
	 */
	@Test
	public void testShouldReturnPitNumberNineAfterGoingThroughPreviousStrategyOptions() {
		int[] theBoard = {0, 0, 0, 0, 1, 2, 6, 5, 4, 3, 0, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 */
	@Test
	public void testShouldReturnPitSevenToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 1, 1, 1, 0, 10, 0, 3, 2, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 */
	@Test
	public void testShouldReturnPitEightToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 1, 1, 1, 3, 0, 10, 3, 2, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test for the situation where the last stone played from a pit will
	 * 	steal an opponent's stones, but only after looping the entire board
	 */
	@Test
	public void testShouldReturnPitNineToLoopTheBoardAndStealOpponentsStones() {
		int[] theBoard = {1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 2, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will land in a pit to steal
	 * 	an opponent's stones - when that pit was empty at the start of the turn
	 * 	and only has a stone because the ComputerPlayer wrapped the board and placed
	 * 	one there
	 */
	@Test
	public void testShouldReturnPitSevenAfterWrappingWhenAdjacentPitWasEmptyAtStartOfTurn() {
		int[] theBoard = {1, 0, 0, 0, 0, 0, 0, 10, 0, 0, 2, 0};
		assertEquals(7, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will land in a pit to steal
	 * 	an opponent's stones - when that pit was empty at the start of the turn
	 * 	and only has a stone because the ComputerPlayer wrapped the board and placed
	 * 	one there
	 */
	@Test
	public void testShouldReturnPitEightAfterWrappingWhenAdjacentPitWasEmptyAtStartOfTurn() {
		int[] theBoard = {1, 0, 0, 0, 0, 0, 8, 0, 10, 0, 3, 0};
		assertEquals(8, this.theStrategy.selectPit(theBoard));
	}
	
	/**
	 * Test situation where the last stone played will land in a pit to steal
	 * 	an opponent's stones - when that pit was empty at the start of the turn
	 * 	and only has a stone because the ComputerPlayer wrapped the board and placed
	 * 	one there
	 */
	@Test
	public void testShouldReturnPitNineAfterWrappingWhenAdjacentPitWasEmptyAtStartOfTurn() {
		int[] theBoard = {1, 0, 0, 0, 0, 0, 9, 8, 0, 10, 4, 0};
		assertEquals(9, this.theStrategy.selectPit(theBoard));
	}
}
