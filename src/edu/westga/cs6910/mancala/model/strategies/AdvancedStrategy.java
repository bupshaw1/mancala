package edu.westga.cs6910.mancala.model.strategies;
/**
 * Strategy for ComputerPlayer behavior in Mancala Application
 * 
 * @author Brandon Lee Upshaw - CS 6910
 * @version July 20th, 2016
 *
 */
public class AdvancedStrategy implements SelectStrategy {

	/**
	 * In this ComputerPlayer strategy, the best move is taken based on 
	 * 	the number of stones in each ComputerPlayer pit, and the number of
	 * 	stones in each Player pit.
	 */
	public AdvancedStrategy() {
		
	}

	@Override
	public int selectPit(int[] theBoard) {
		int closestPit = theBoard.length - 2;
		int lastPitChoice = theBoard.length / 2;
		int computerStore = theBoard.length - 1;
	// Phase 1
		while (closestPit >= lastPitChoice) {
			if (this.findFinalPit(theBoard, closestPit) == computerStore) {
				return closestPit;
			}
			closestPit--;
		}
		
	// Phase 2	
		closestPit = theBoard.length - 2;
		while (closestPit >= lastPitChoice) {
			int[] currentBoard = this.cloneBoard(theBoard);
			if (theBoard[closestPit] != 0 && this.pitCanSteal(currentBoard, closestPit)) {
				return closestPit;
			}
			closestPit--;
		}
		
	// Phase 3
		return this.selectClosestPitWithStones(theBoard);
	}
	
	private int[] cloneBoard(int[] theBoard) {
		int[] clonedArray = theBoard.clone();
		return clonedArray;
	}
	
	private int findFinalPit(int[] theBoard, int currentPit) {
		int currentStones = theBoard[currentPit];
		int selectedPit = currentPit;
		while (currentStones > 0) {
			currentStones--;
			selectedPit++;
			if (selectedPit == theBoard.length) {
				selectedPit = 0;
			}
			if (selectedPit == ((theBoard.length / 2) - 1)) {
				currentStones++;
			}
			
		}
		return selectedPit;
	}
	
	private boolean pitCanSteal(int[] currentBoard, int currentPit) {
		int currentStones = currentBoard[currentPit];
		currentBoard[currentPit] = 0;
		int selectedPit = currentPit;
		int lastPitValue = 0;
		
		while (currentStones > 0) {
			selectedPit++;
			if (selectedPit == currentBoard.length) {
				selectedPit = 0;
			}
			if (selectedPit == (currentBoard.length / 2 - 1)) {
				selectedPit++;
			}
			currentStones--;
			lastPitValue = currentBoard[selectedPit];
			currentBoard[selectedPit]++;	
		}
		int adjacentPit = (currentBoard.length - 2) - selectedPit;
		return !(currentBoard[adjacentPit] == 0 || selectedPit < (currentBoard.length / 2) || lastPitValue != 0);
	}

	private int selectClosestPitWithStones(int[] theBoard) {
		int closestPit = theBoard.length - 2;
		while (theBoard[closestPit] == 0) {
			--closestPit;
		}
		return closestPit;
	}	

}
